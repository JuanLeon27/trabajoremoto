package practicaentornos;

import java.util.Scanner;

/**
 *
 * @author Juan Antonio Sanchez Leon, Alessandro Sinibaldi <1º DAW>
 */
public class PracticaEntornos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int opcion = 0;
        double kms;
        double ms;
        double importe;
        int numero;
	        opcion = menu(opcion);

        switch(opcion){

            case 1: System.out.println("Introduce el numero");

                    numero = sc.nextInt();

                    System.out.print("Factorial de " + numero + " = ");

                    calcularFactorial(numero);

                    break;

            case 2: System.out.println("Introduce los kilometros por hora");

                    kms = sc.nextDouble();

                    kilometrosAmetros(kms);

                    break;

            case 3: System.out.println("Introduce los kilometros por segundo");

                    ms = sc.nextDouble();

                    metrosAkilometros(ms);

                    break;

            case 4: sumarValores();

                    break;

            case 5: System.out.println("Introduce el importe");

                    importe = sc.nextDouble();

                    anyadirIVA(importe);

                    break;
        }

    }
    public static int menu(int opcion){

        Scanner sc = new Scanner(System.in);

        do{

            System.out.println("Selecciona una opcion");

            System.out.println("1. CalcularFactorial");

            System.out.println("2. Convertir kilometros por hora a metros por segundo");

            System.out.println("3. Convertir metros por segundo a kilometros por hora");

            System.out.println("4. Sumar valores");

            System.out.println("5. Añadir IVA al importe de los productos");

            opcion = sc.nextInt();

            if(opcion <= 0 && opcion > 5){

                System.out.println("Esa opcion no esta disponible");

            }

        }while(opcion <= 0 && opcion > 5);

        return opcion;

    }

public static void calcularFactorial(int numero){

        int i;

        for(i=(numero-1);i>1;i--){

            numero = numero * i;

        }

        System.out.print(numero);

        System.out.println();

    }

public static void kilometrosAmetros(double kms){

        double ms = kms * 1000 / 3600;

        System.out.println(kms + " km/h = " + ms + " m/s");

    }
 public static void metrosAkilometros(double ms){

        double kms = ms / 3600 * 1000;

        System.out.println(ms + " m/s = " + kms + " km/h");

    }



    public static void sumarValores(){

        Scanner sc = new Scanner(System.in);

        int i;

        int tamanyo;

        double numero;

        double sumaTodosLosValores = 0;

        System.out.println("Cuantos numeros quieres sumar?");

        tamanyo = sc.nextInt();

        sc.nextLine();

        System.out.println("Ingresa los numeros que quieres sumar");

        for(i=0;i<tamanyo;i++){

            numero = sc.nextDouble();

            sumaTodosLosValores = sumaTodosLosValores + numero;

   	}

        System.out.println("La suma de todos los numeros es: " + sumaTodosLosValores);
    }

public static void anyadirIVA(double importeTotal){
        double importeConIVA = importeTotal + (importeTotal * 0.21);
        System.out.println("Importe con IVA = " + importeConIVA);
    }

}
